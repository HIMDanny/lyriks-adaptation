# Project Lyriks

This is my implementation of educational project.

## APIs

- [Shazam Core](https://rapidapi.com/tipsters/api/shazam-core)
- [IP Geolocation](https://geo.ipify.org/)

## Demo

[Lyriks](https://hd-lyriks-app.netlify.app/)
